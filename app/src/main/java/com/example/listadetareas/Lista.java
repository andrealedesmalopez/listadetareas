package com.example.listadetareas;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Lista extends AppCompatActivity {
    ListView lista;
    Button nueva;
    ArrayList<String> tarea;
    //adaptador: todos los controles de selección accederán a los datos que contienen a través de este.
   ;ArrayAdapter<String> adaptador;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_lista);
        nueva=(Button)findViewById(R.id.btnNuevaT);
        lista = (ListView) findViewById(R.id.listViewTareas);

        tarea = new ArrayList<String>();
        tarea  = (ArrayList<String>)getIntent().getSerializableExtra("NuevaTarea");

        if(tarea.get(0).equals("")){
            Toast.makeText(Lista.this, "No hay tareas", Toast.LENGTH_SHORT).show();
            adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, tarea);
            lista.setAdapter(adaptador);
        }else {
            adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, tarea);
            Toast.makeText(Lista.this, "Tarea nueva agreagada", Toast.LENGTH_SHORT).show();
            lista.setAdapter(adaptador);
        }

        nueva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        //para tarea completada
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(Lista.this, "Tarea completada", Toast.LENGTH_SHORT).show();
            }
        });

    }
}