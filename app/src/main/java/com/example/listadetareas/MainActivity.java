package com.example.listadetareas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
Button agregar, verLista;
EditText titulo, desc;
Editable tituloT, descT;
String tituloTarea, descTarea;
ArrayList<String> nTarea;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        agregar=(Button)findViewById(R.id.btnAgregar);
        verLista=(Button)findViewById(R.id.btnVerLista);
        titulo=(EditText)findViewById(R.id.etxtTitulo);
        desc=(EditText)findViewById(R.id.etxtDesc);
        nTarea = new ArrayList<String>();

        verLista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                nTarea.add("");
                Intent verL = new Intent(v.getContext(), Lista.class);
                verL.putExtra("NuevaTarea", nTarea);
                startActivity(verL);
            }
        });
        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                tituloT=titulo.getText();
                descT=desc.getText();
                tituloTarea=String.valueOf(tituloT);
                descTarea=String.valueOf(desc);
                if(tituloTarea.isEmpty()||descTarea.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Ingresa el título", Toast.LENGTH_LONG).show();
                }else {
                    nTarea.add(String.valueOf(tituloT).concat(": " + descT));
                    Intent nuevaT = new Intent(v.getContext(), Lista.class);
                    nuevaT.putExtra("NuevaTarea", nTarea);
                    startActivity(nuevaT);
                }
            }
        });
    }
}